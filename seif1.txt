create table students (studentID INT, studentName TEXT, studentAge TEXT, studentGrade TEXT);
create table classes (classID INT, className TEXT, teacherID INT, catID INT);
create table categories (catID INT, catName TEXT);
create table teachers (teacherID INT, teacherName TEXT);
create table studentsInClasses (studentID INT, classID INT); 